#include <iostream>
using namespace std;

class Bird{
public:
virtual void fly() = 0;
};
class Penguin:public Bird{
public:
  void fly(){
 std::cout<<"I'm a Penguin I can't fly"<<std::endl;
}
};

class OtherBird:public Bird{
public:
   void fly() {
  std::cout<<"I'm a bird I can fly"<<std::endl;
  }
};

Bird* createBird(bool bflag){
 if(bflag){
    return new OtherBird();
 }
    else{
       return new Penguin();
  }
}


int main()
{
 Bird* bird = createBird(true);
 bird->fly();
 Bird* penguin = createBird(false);
 penguin->fly();
 return 0;
}
