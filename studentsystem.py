import smtplib

students = []

def add_student(roll_number, name, age, email):
    student = {
        'roll_number': roll_number,
        'name': name,
        'age': age,
        'email': email
    }
    students.append(student)
    print("Student added successfully.")

def remove_student(roll_number):
    for student in students:
        if student['roll_number'] == roll_number:
            students.remove(student)
            print("Student removed successfully.")
            return
    print("Student not found.")

def display_students():
    print("Student List:")
    if len(students) == 0:
        print("No students found.")
    else:
        for student in students:
            print("Roll Number:", student['roll_number'])
            print("Name:", student['name'])
            print("Age:", student['age'])
            print("Email:", student['email'])
            print()

def send_email(smtp_server, smtp_port, sender_email, sender_password, recipient_email, subject, body):
    try:
        server = smtplib.SMTP(smtp_server, smtp_port)
        server.starttls()
        server.login(sender_email, sender_password)

        message = f"Subject: {subject}\n\n{body}"
        server.sendmail(sender_email, recipient_email, message)

        print("Email sent successfully.")
    except Exception as e:
        print("An error occurred while sending the email:", str(e))
    finally:
        server.quit()

while True:
    print("\nStudent Management System")
    print("------------------------")
    print("1. Add Student")
    print("2. Remove Student")
    print("3. Display Students")
    print("4. Send Email to Student")
    print("5. Exit")
    choice = input("Enter your choice: ")

    if choice == '1':
        roll_number = int(input("Enter roll number: "))
        name = input("Enter name: ")
        age = int(input("Enter age: "))
        email = input("Enter email: ")
        add_student(roll_number, name, age, email)
    elif choice == '2':
        roll_number = int(input("Enter roll number to remove: "))
        remove_student(roll_number)
    elif choice == '3':
        display_students()
    elif choice == '4':
        smtp_server = input("Enter SMTP server: ")
        smtp_port = int(input("Enter SMTP port: "))
        sender_email = input("Enter sender email: ")
        sender_password = input("Enter sender password: ")
        recipient_email = input("Enter recipient email: ")
        subject = input("Enter email subject: ")
        body = input("Enter email body: ")
        send_email(smtp_server, smtp_port, sender_email, sender_password, recipient_email, subject, body)
    elif choice == '5':
        print("Exiting the system. Goodbye!")
        break
    else:
        print("Invalid choice. Please try again.")
