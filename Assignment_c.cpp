#include <iostream>
using namespace std;

class Bird{
public:
virtual void fly(){
 cout<<"I'm a bird I can fly"<<endl;
}
};
class Penguin:public Bird{
public:
void fly(){
 cout<<"I'm a Penguin I can't fly"<<endl;
}
};

int main()
{
 Bird bird;
 Penguin penguin;
 bird.fly();
 penguin.fly();
 return 0;
}


