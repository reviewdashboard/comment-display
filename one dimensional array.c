#include <stdio.h>

int main()
{
   int arr[] = {12, 43, 54, 7, 120, 430};
   int i, tot;

   printf("The first element = %d", arr[0]);
   printf("\nThe second element = %d", arr[1]);

   tot = sizeof(arr)/sizeof(arr[0]);
   printf("\nThe last element = %d", arr[tot-1]);

   return 0;
}